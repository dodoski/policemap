package sample;

import javafx.animation.Animation;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.awt.*;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        URL url = new URL()
        Image image = new Image();

        Circle circle = new Circle();
        Circle circle1 = new Circle();
        circle1.setFill(Color.RED);
        circle1.setRadius(9);
        circle1.setLayoutX(500);
        circle1.setLayoutY(500);
        circle1.relocate(500, 500);
        circle.setFill(Color.BLUE);
        circle.setRadius(9);
        circle.setLayoutX(50);
        circle.setLayoutY(50);
        circle.relocate(50, 50);

        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.seconds(3));
        transition.setToX(440);
        transition.setToY(440);
        transition.setNode(circle);
        transition.setAutoReverse(true);
        transition.setCycleCount(1);
        transition.play();
        BorderPane root = new BorderPane();
        root.getChildren().addAll(circle, circle1);
        root.setBackground(new Background(new BackgroundImage(image, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, BackgroundSize.DEFAULT)));
        Scene scene = new Scene(root, 600,600);



        primaryStage.setTitle("Transition");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
